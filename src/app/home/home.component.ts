import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  data = [
    { title: 'Flexibility', image: "/assets/images/s1.jpg" },
    { title: 'Health', image: "/assets/images/s2.jpg" },
    { title: 'Relaxation', image: "/assets/images/s3.jpg" },
    { title: 'Support', image: "/assets/images/s4.jpg" },
    { title: 'Health', image: "/assets/images/s5.jpg" },
    { title: 'Spirituality', image: "/assets/images/s6.jpg" },
  ]

  eventData = [
    {
      title: 'Yoga Lite for Pregent Women only',
      date: 'Feb 03.2019',
      content: 'Lorem ipsum dolor sit amet, consectetur adipisicing eiusmod tempor incididunt ut labore et dolore magna.',
      image: '/assets/images/n1.jpg',
      reverseImage: true
    },
    {
      title: 'Elderly Yoga Lite for Every one',
      date: 'Feb 06.2019',
      content: 'Lorem ipsum dolor sit amet, consectetur adipisicing eiusmod tempor incididunt ut labore et dolore magna.',
      image: '/assets/images/n2.jpg',
      reverseImage: false
    },
    {
      title: 'Doing Pilates for Busy People',
      date: 'Feb 07.2019',
      content: 'Lorem ipsum dolor sit amet, consectetur adipisicing eiusmod tempor incididunt ut labore et dolore magna.',
      image: '/assets/images/n3.jpg',
      reverseImage: true
    },
  ]

  constructor() { }

  ngOnInit() {
  }

}
