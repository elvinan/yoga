import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup
  constructor() { }

  ngOnInit() {

  }

  onLogin() {
    alert('Your email: ' + (<HTMLInputElement>document.getElementById('exampleInputEmail1')).value +
      '\nYour password: ' + (<HTMLInputElement>document.getElementById('exampleInputPassword1')).value)
  }

}
